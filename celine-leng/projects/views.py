from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project, Art

def home(request):
    return render(request, "projects/home.html")

def work(request):
     projects = Project.objects.all()
     context = {
        "projects": projects,
     }
     return render(request, "projects/work.html", context)

def work_detail(request, id):
   art = get_object_or_404(Project, id=id)
   context = {
      "art": art,
   }
   return render(request, "projects/work_detail.html", context)

def contact(request):
   return render(request, "projects/contact.html")

def about(request):
   return render(request, "projects/about.html")
