from django.contrib import admin
from projects.models import Project, Art

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

@admin.register(Art)
class ArtAdmin(admin.ModelAdmin):
    list_display = (
        "project_art",
        "order",
        "id"
    )
