from django.db import models
import os

class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    picture = models.ImageField(upload_to="Images")

    def __str__(self):
        return self.title

class Art(models.Model):
    order = models.PositiveBigIntegerField()
    picture = models.ImageField(upload_to='Images')
    art = models.ForeignKey(
        "Project",
        related_name="artwork",
        on_delete=models.CASCADE
    )

    def project_art(self):
        return os.path.basename(self.picture.name)

    class Meta:
        ordering = ["order"]
