from django.urls import path
from projects.views import home, work, work_detail, about, contact

urlpatterns = [
    path("", home, name="home"),
    path("work/", work, name="work"),
    path("contact/", contact, name="contact"),
    path("about/", about, name="about"),
    path("work/<int:id>/", work_detail, name="work_detail"),
]
